﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TxtSubTransporte = New System.Windows.Forms.TextBox()
        Me.TxtSueldo = New System.Windows.Forms.TextBox()
        Me.LblTotalDevengos = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TxtPension = New System.Windows.Forms.TextBox()
        Me.TxtSalud = New System.Windows.Forms.TextBox()
        Me.LblTotalDeducciones = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.LblTotalPagar = New System.Windows.Forms.Label()
        Me.BtnCalcularLiquidacion = New System.Windows.Forms.Button()
        Me.BtnLimpiar = New System.Windows.Forms.Button()
        Me.BtnVerAcumulado = New System.Windows.Forms.Button()
        Me.TxtIdentificacion = New System.Windows.Forms.TextBox()
        Me.TxtSalarioBasico = New System.Windows.Forms.TextBox()
        Me.TxtDiasTrabajados = New System.Windows.Forms.TextBox()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TxtNombre = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(278, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(190, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Liquidación de Nómina"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(36, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Identificación"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(36, 94)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Nombre"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(395, 67)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(110, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Salario Básico"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(395, 94)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(125, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Días Trabajados"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TxtSubTransporte)
        Me.GroupBox1.Controls.Add(Me.TxtSueldo)
        Me.GroupBox1.Controls.Add(Me.LblTotalDevengos)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(38, 122)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(328, 134)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Devengos"
        '
        'TxtSubTransporte
        '
        Me.TxtSubTransporte.Enabled = False
        Me.TxtSubTransporte.Location = New System.Drawing.Point(161, 58)
        Me.TxtSubTransporte.Name = "TxtSubTransporte"
        Me.TxtSubTransporte.Size = New System.Drawing.Size(149, 24)
        Me.TxtSubTransporte.TabIndex = 19
        '
        'TxtSueldo
        '
        Me.TxtSueldo.Enabled = False
        Me.TxtSueldo.Location = New System.Drawing.Point(161, 30)
        Me.TxtSueldo.Name = "TxtSueldo"
        Me.TxtSueldo.Size = New System.Drawing.Size(149, 24)
        Me.TxtSueldo.TabIndex = 18
        '
        'LblTotalDevengos
        '
        Me.LblTotalDevengos.AutoSize = True
        Me.LblTotalDevengos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTotalDevengos.Location = New System.Drawing.Point(158, 100)
        Me.LblTotalDevengos.Name = "LblTotalDevengos"
        Me.LblTotalDevengos.Size = New System.Drawing.Size(16, 16)
        Me.LblTotalDevengos.TabIndex = 9
        Me.LblTotalDevengos.Text = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(6, 100)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(119, 16)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Total Devengos"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(6, 63)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(149, 16)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Subsidio Transporte"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 30)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(57, 16)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Sueldo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TxtPension)
        Me.GroupBox2.Controls.Add(Me.TxtSalud)
        Me.GroupBox2.Controls.Add(Me.LblTotalDeducciones)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(398, 122)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(334, 134)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Deducciones"
        '
        'TxtPension
        '
        Me.TxtPension.Enabled = False
        Me.TxtPension.Location = New System.Drawing.Point(165, 58)
        Me.TxtPension.Name = "TxtPension"
        Me.TxtPension.Size = New System.Drawing.Size(149, 24)
        Me.TxtPension.TabIndex = 20
        '
        'TxtSalud
        '
        Me.TxtSalud.Enabled = False
        Me.TxtSalud.Location = New System.Drawing.Point(165, 22)
        Me.TxtSalud.Name = "TxtSalud"
        Me.TxtSalud.Size = New System.Drawing.Size(149, 24)
        Me.TxtSalud.TabIndex = 19
        '
        'LblTotalDeducciones
        '
        Me.LblTotalDeducciones.AutoSize = True
        Me.LblTotalDeducciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTotalDeducciones.Location = New System.Drawing.Point(162, 100)
        Me.LblTotalDeducciones.Name = "LblTotalDeducciones"
        Me.LblTotalDeducciones.Size = New System.Drawing.Size(16, 16)
        Me.LblTotalDeducciones.TabIndex = 9
        Me.LblTotalDeducciones.Text = "0"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(6, 100)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(139, 16)
        Me.Label13.TabIndex = 7
        Me.Label13.Text = "Total Deducciones"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(6, 63)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 16)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Pensión"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(6, 30)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 16)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "Salud"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(239, 272)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(127, 16)
        Me.Label16.TabIndex = 7
        Me.Label16.Text = "TOTAL A PAGAR"
        '
        'LblTotalPagar
        '
        Me.LblTotalPagar.AutoSize = True
        Me.LblTotalPagar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTotalPagar.Location = New System.Drawing.Point(394, 272)
        Me.LblTotalPagar.Name = "LblTotalPagar"
        Me.LblTotalPagar.Size = New System.Drawing.Size(19, 20)
        Me.LblTotalPagar.TabIndex = 8
        Me.LblTotalPagar.Text = "0"
        '
        'BtnCalcularLiquidacion
        '
        Me.BtnCalcularLiquidacion.BackColor = System.Drawing.Color.DarkGray
        Me.BtnCalcularLiquidacion.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnCalcularLiquidacion.FlatAppearance.BorderSize = 0
        Me.BtnCalcularLiquidacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCalcularLiquidacion.ForeColor = System.Drawing.Color.Transparent
        Me.BtnCalcularLiquidacion.Location = New System.Drawing.Point(295, 300)
        Me.BtnCalcularLiquidacion.Name = "BtnCalcularLiquidacion"
        Me.BtnCalcularLiquidacion.Size = New System.Drawing.Size(188, 37)
        Me.BtnCalcularLiquidacion.TabIndex = 10
        Me.BtnCalcularLiquidacion.Text = "Calcular"
        Me.BtnCalcularLiquidacion.UseVisualStyleBackColor = False
        '
        'BtnLimpiar
        '
        Me.BtnLimpiar.BackColor = System.Drawing.Color.DarkGray
        Me.BtnLimpiar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnLimpiar.FlatAppearance.BorderSize = 0
        Me.BtnLimpiar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnLimpiar.ForeColor = System.Drawing.Color.Transparent
        Me.BtnLimpiar.Location = New System.Drawing.Point(39, 300)
        Me.BtnLimpiar.Name = "BtnLimpiar"
        Me.BtnLimpiar.Size = New System.Drawing.Size(188, 37)
        Me.BtnLimpiar.TabIndex = 12
        Me.BtnLimpiar.Text = "Limpiar"
        Me.BtnLimpiar.UseVisualStyleBackColor = False
        '
        'BtnVerAcumulado
        '
        Me.BtnVerAcumulado.BackColor = System.Drawing.Color.DarkGray
        Me.BtnVerAcumulado.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnVerAcumulado.FlatAppearance.BorderSize = 0
        Me.BtnVerAcumulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnVerAcumulado.ForeColor = System.Drawing.Color.Transparent
        Me.BtnVerAcumulado.Location = New System.Drawing.Point(546, 300)
        Me.BtnVerAcumulado.Name = "BtnVerAcumulado"
        Me.BtnVerAcumulado.Size = New System.Drawing.Size(188, 37)
        Me.BtnVerAcumulado.TabIndex = 13
        Me.BtnVerAcumulado.Text = "Ver Acumulado"
        Me.BtnVerAcumulado.UseVisualStyleBackColor = False
        '
        'TxtIdentificacion
        '
        Me.TxtIdentificacion.Location = New System.Drawing.Point(166, 63)
        Me.TxtIdentificacion.Name = "TxtIdentificacion"
        Me.TxtIdentificacion.Size = New System.Drawing.Size(200, 20)
        Me.TxtIdentificacion.TabIndex = 14
        '
        'TxtSalarioBasico
        '
        Me.TxtSalarioBasico.Location = New System.Drawing.Point(546, 63)
        Me.TxtSalarioBasico.Name = "TxtSalarioBasico"
        Me.TxtSalarioBasico.Size = New System.Drawing.Size(186, 20)
        Me.TxtSalarioBasico.TabIndex = 16
        '
        'TxtDiasTrabajados
        '
        Me.TxtDiasTrabajados.Location = New System.Drawing.Point(546, 96)
        Me.TxtDiasTrabajados.MaxLength = 2
        Me.TxtDiasTrabajados.Name = "TxtDiasTrabajados"
        Me.TxtDiasTrabajados.Size = New System.Drawing.Size(186, 20)
        Me.TxtDiasTrabajados.TabIndex = 17
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'TxtNombre
        '
        Me.TxtNombre.Location = New System.Drawing.Point(166, 96)
        Me.TxtNombre.Name = "TxtNombre"
        Me.TxtNombre.Size = New System.Drawing.Size(200, 20)
        Me.TxtNombre.TabIndex = 15
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkGray
        Me.ClientSize = New System.Drawing.Size(763, 344)
        Me.Controls.Add(Me.TxtDiasTrabajados)
        Me.Controls.Add(Me.TxtSalarioBasico)
        Me.Controls.Add(Me.TxtNombre)
        Me.Controls.Add(Me.TxtIdentificacion)
        Me.Controls.Add(Me.BtnVerAcumulado)
        Me.Controls.Add(Me.BtnLimpiar)
        Me.Controls.Add(Me.BtnCalcularLiquidacion)
        Me.Controls.Add(Me.LblTotalPagar)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Name = "Form1"
        Me.Text = "Liquidación de Nómina"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents LblTotalDevengos As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents LblTotalDeducciones As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents LblTotalPagar As System.Windows.Forms.Label
    Friend WithEvents BtnCalcularLiquidacion As System.Windows.Forms.Button
    Friend WithEvents BtnLimpiar As System.Windows.Forms.Button
    Friend WithEvents BtnVerAcumulado As System.Windows.Forms.Button
    Friend WithEvents TxtSubTransporte As System.Windows.Forms.TextBox
    Friend WithEvents TxtSueldo As System.Windows.Forms.TextBox
    Friend WithEvents TxtPension As System.Windows.Forms.TextBox
    Friend WithEvents TxtSalud As System.Windows.Forms.TextBox
    Friend WithEvents TxtIdentificacion As System.Windows.Forms.TextBox
    Friend WithEvents TxtSalarioBasico As System.Windows.Forms.TextBox
    Friend WithEvents TxtDiasTrabajados As System.Windows.Forms.TextBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents TxtNombre As System.Windows.Forms.TextBox

End Class
