﻿Public Class Form1
    Private Const APORTESS = 4
    Private Const SML = 616000
    Private Const AUXTRASPORTE = 72000
    Private acumulado, pension, salud, subTransporte, sueldo, totalDeducciones, totalDevengos, totalPagar As Double
    
    
    'Estilos para los botones
    Private Sub CalcularLiquidacion_MouseHover(sender As System.Object, e As System.EventArgs) Handles BtnCalcularLiquidacion.MouseHover
        BtnCalcularLiquidacion.BackColor = Color.DarkGreen
    End Sub

    Private Sub CalcularLiquidacion_MouseLeave(sender As System.Object, e As System.EventArgs) Handles BtnCalcularLiquidacion.MouseLeave
        BtnCalcularLiquidacion.BackColor = Color.DarkGray
    End Sub

    Private Sub BtnVerAcumulado_MouseHover(sender As System.Object, e As System.EventArgs) Handles BtnVerAcumulado.MouseHover
        BtnVerAcumulado.BackColor = Color.DarkGreen
    End Sub

    Private Sub BtnVerAcumulado_MouseLeave(sender As System.Object, e As System.EventArgs) Handles BtnVerAcumulado.MouseLeave
        BtnVerAcumulado.BackColor = Color.DarkGray
    End Sub

    Private Sub BtnLimpiar_MouseHover(sender As System.Object, e As System.EventArgs) Handles BtnLimpiar.MouseHover
        BtnLimpiar.BackColor = Color.LightGray
    End Sub

    Private Sub BtnLimpiar_MouseLeave(sender As System.Object, e As System.EventArgs) Handles BtnLimpiar.MouseLeave
        BtnLimpiar.BackColor = Color.DarkGray
    End Sub

    'Al hacer click sobre el boton Calcular como ya se tenemos los valores de Sueldo, subsidio de transporte,
    'salud y pension, entoces podemos calcular los totales de Devengo, Deduccion y con estos dos totales
    'optenemos el total a pagar e ir sumando al acumulado el valor del total a pagar
    Private Sub BtnCalcularLiquidacion_Click(sender As System.Object, e As System.EventArgs) Handles BtnCalcularLiquidacion.Click
        If errors() = True Then
            Me.totalDevengos = Me.sueldo + Me.subTransporte
            Me.totalDeducciones = Me.salud + Me.pension
            Me.totalPagar = Me.totalDevengos - Me.totalDeducciones
            Me.acumulado += Me.totalPagar
            LblTotalDevengos.Text = Me.totalDevengos.ToString("$###,###,###.00;($###,###,###.00)")
            LblTotalDeducciones.Text = Me.totalDeducciones.ToString("$###,###,###.00;($###,###,###.00)")
            LblTotalPagar.Text = Me.totalPagar.ToString("$###,###,###.00;($###,###,###.00)")
        End If
    End Sub
    

    'Muestra en un mensaje el valor del acumulado
    Private Sub BtnVerAcumulado_Click(sender As System.Object, e As System.EventArgs) Handles BtnVerAcumulado.Click
        MsgBox(acumulado.ToString("$###,###,###.00;($###,###,###.00)")
)
    End Sub

    'Eventos KeyUp hacen el llamado a la funsión calcularDevengadosDescuentos()
    Private Sub TxtSalarioBasico_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles TxtSalarioBasico.KeyUp
        If Val(TxtDiasTrabajados.Text) <= 31 And Val(TxtDiasTrabajados.Text) > 0 Then
            resetTotales()
            calcularDevengadosDescuentos()
        End If
    End Sub

    Private Sub TxtDiasTrabajados_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles TxtDiasTrabajados.KeyUp
        If Val(TxtDiasTrabajados.Text) <= 31 And Val(TxtDiasTrabajados.Text) > 0 Then
            resetTotales()
            calcularDevengadosDescuentos()
            ErrorProvider1.SetError(TxtDiasTrabajados, Nothing)
        Else
            ErrorProvider1.SetError(TxtDiasTrabajados, "Digite un número entre 1 y 31")
        End If

    End Sub

    'Evento Change. Se encarga de limpiar el campo de texto  y asignar valor 0 al subsidio de transporte
    'en caso de que el empleado devengue más de dos salarios mínimos
    Private Sub TxtSueldo_TextChanged(sender As System.Object, e As EventArgs) Handles TxtSueldo.TextChanged
        
    End Sub

    'Evento KeyPress. Evita que se digiten números en el campo nombre
    Private Sub TxtNombre_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxtNombre.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    'Funsión encargada de validar los datos de cada campo
    Private Function errors() As Boolean
        If TxtIdentificacion.Text = "" Then
            ErrorProvider1.SetError(TxtIdentificacion, "Digite un número de identificación")
            Return False
        Else
            ErrorProvider1.SetError(TxtIdentificacion, Nothing)
        End If
        If TxtNombre.Text = "" Then
            ErrorProvider1.SetError(TxtNombre, "Digite un nombre")
            Return False
        Else
            ErrorProvider1.SetError(TxtNombre, Nothing)
        End If

        If IsNumeric(TxtSalarioBasico.Text) Then
            ErrorProvider1.SetError(TxtSalarioBasico, Nothing)
        Else
            ErrorProvider1.SetError(TxtSalarioBasico, "Tipo de dato no valido. Digite solo números")
            Return False
        End If
        If IsNumeric(TxtDiasTrabajados.Text) Then
            ErrorProvider1.SetError(TxtDiasTrabajados, Nothing)
        Else
            ErrorProvider1.SetError(TxtDiasTrabajados, "Tipo de dato no valido. Digite solo números")
            Return False
        End If
        If IsNumeric(TxtSueldo.Text) Then
            ErrorProvider1.SetError(TxtSueldo, Nothing)
        Else
            ErrorProvider1.SetError(TxtSueldo, "Tipo de dato no valido. Digite solo números")
            Return False
        End If
        If IsNumeric(TxtSueldo.Text) Then
            ErrorProvider1.SetError(TxtSueldo, Nothing)
        Else
            ErrorProvider1.SetError(TxtSueldo, "Tipo de dato no valido. Digite solo números")
            Return False
        End If
        If Val(TxtDiasTrabajados.Text) <= 31 And Val(TxtDiasTrabajados.Text) > 0 Then
            ErrorProvider1.SetError(TxtDiasTrabajados, Nothing)
        Else
            ErrorProvider1.SetError(TxtDiasTrabajados, "Digite un número entre 1 y 31")
            Return False
        End If

        Return True
    End Function

    'funsión encargada de calcular Devengos (Sueldo y subsidio de transporte) y Descuentos (Salud y pension)
    Sub calcularDevengadosDescuentos()
        If TxtSalarioBasico.Text = "" And TxtDiasTrabajados.Text = "" Then
            If TxtSalarioBasico.Text = "" Then
                ErrorProvider1.SetError(TxtSalarioBasico, "Se requiere un valor para el salario básico")
            End If
            If TxtDiasTrabajados.Text = "" Then
                ErrorProvider1.SetError(TxtDiasTrabajados, "Se requiere un valor para días trabajados")
            End If
            TxtSueldo.Text = ""
        Else
            ErrorProvider1.SetError(TxtSalarioBasico, Nothing)
            ErrorProvider1.SetError(TxtDiasTrabajados, Nothing)
            If IsNumeric(TxtSalarioBasico.Text) And IsNumeric(TxtDiasTrabajados.Text) Then
                ErrorProvider1.SetError(TxtSalarioBasico, Nothing)
                ErrorProvider1.SetError(TxtDiasTrabajados, Nothing)
                If Val(TxtDiasTrabajados.Text) > 0 And Val(TxtDiasTrabajados.Text) <= 31 Then
                    Me.sueldo = (Val(TxtSalarioBasico.Text) * Val(TxtDiasTrabajados.Text)) / 30
                    TxtSueldo.Text = Math.Round(Me.sueldo, 2)
                    Me.salud = (Me.sueldo * APORTESS) / 100
                    TxtSalud.Text = Math.Round(Me.salud, 2)
                    Me.pension = (Me.sueldo * APORTESS) / 100
                    TxtPension.Text = Math.Round(Me.pension, 2)
                    If Val(TxtSueldo.Text) > SML * 2 Then
                        TxtSubTransporte.Text = ""
                        Me.subTransporte = 0
                    Else
                        Me.subTransporte = (AUXTRASPORTE / 30) * Val(TxtDiasTrabajados.Text)
                        TxtSubTransporte.Text = Math.Round(Me.subTransporte, 2)
                        'TxtSubTransporte.Text = (AUXTRASPORTE * 30) / Val(TxtDiasTrabajados.Text)
                    End If
                    
                End If
            Else
                If Not IsNumeric(TxtSalarioBasico.Text) Then
                    ErrorProvider1.SetError(TxtSalarioBasico, "Tipo de dato no valido. Digite solo números")

                End If
                If Not IsNumeric(TxtDiasTrabajados.Text) Then
                    ErrorProvider1.SetError(TxtDiasTrabajados, "Tipo de dato no valido. Digite solo números entre 1 y 31")
                End If
                TxtSueldo.Text = ""
            End If
        End If

        If IsNumeric(TxtSalarioBasico.Text) Then
            ErrorProvider1.SetError(TxtSalarioBasico, Nothing)
            If Val(TxtDiasTrabajados.Text) > 0 Then
                Me.sueldo = (Val(TxtSalarioBasico.Text) * Val(TxtDiasTrabajados.Text)) / 30
                TxtSueldo.Text = Math.Round(Me.sueldo, 2)
            End If
        Else
            ErrorProvider1.SetError(TxtSalarioBasico, "Tipo de dato no valido. Digite solo números")
        End If
    End Sub

    'Funsión encargada de resetear el valor de los campos del formulario
    Private Sub BtnLimpiar_Click(sender As System.Object, e As System.EventArgs) Handles BtnLimpiar.Click
        TxtNombre.Text = ""
        TxtIdentificacion.Text = ""
        TxtSalarioBasico.Text = ""
        TxtDiasTrabajados.Text = ""
        TxtSueldo.Text = ""
        TxtSubTransporte.Text = ""
        LblTotalDevengos.Text = ""
        TxtSalud.Text = ""
        TxtPension.Text = ""
        LblTotalDeducciones.Text = ""
        LblTotalPagar.Text = ""
    End Sub
    Private Sub resetTotales()
        Me.totalDevengos = 0
        Me.totalDeducciones = 0
        Me.totalPagar = 0
        LblTotalDevengos.Text = "0"
        LblTotalDeducciones.Text = "0"
        LblTotalPagar.Text = "0"
    End Sub
End Class
